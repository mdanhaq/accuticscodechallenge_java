# AccuticsCodeChallenge_Java

Accutics:  Backend Developer - Code Challenge

## Project Overview

This project has 3 entity (users, inputs & campaigns) with the purpose of: 
- Implement an endpoint that returns a list of campaigns
- Implement an endpoint for listing users
- Implement an endpoint creating a campaign

## Requirements
IntelliJ IDEA as IDE 

## Installation
To run this project, someone needs Spring Boot with Spring Web, Spring Data JPA and PostgreSQL Driver as dependency. I have used Spring Initializr from https://start.spring.io/ to make jar file and then proceeded with that one. For maintaining the database PostgreSQL was my choice.

## Description

This project is crated for having JSON-based APIs for backend services and has no visual interface. Created endpoints returns json data and those endpoints are as follows:

User Endpoints:
http://localhost:8082/api/v1/users

Campaigns Endpoints:
http://localhost:8082/api/v1/campaigns

Input Endpoints:
http://localhost:8082/api/v1/inputs

Get user using specific email:
http://localhost:8082/api/v1/users/mdanhaq@gmail.com

Get campaign with the pagination requestparams
http://localhost:8082/api/v1/campaigns?page=1

Get campaign with the sortBy requestparams
http://localhost:8082/api/v1/campaigns?soryBy=campaignId


