package com.ansarul.AccuticsCodeChallenge.repositories;

import com.ansarul.AccuticsCodeChallenge.models.Users;
import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
    List<Users> getUserByEmail(String email);
}
