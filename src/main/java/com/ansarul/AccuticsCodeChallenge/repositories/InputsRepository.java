package com.ansarul.AccuticsCodeChallenge.repositories;

import com.ansarul.AccuticsCodeChallenge.models.Inputs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputsRepository extends JpaRepository<Inputs, Integer> {
}
