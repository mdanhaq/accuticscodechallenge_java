package com.ansarul.AccuticsCodeChallenge.repositories;

import com.ansarul.AccuticsCodeChallenge.models.Campaigns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignsRepository extends JpaRepository<Campaigns, Integer> {
}
