package com.ansarul.AccuticsCodeChallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccuticsCodeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccuticsCodeChallengeApplication.class, args);
	}

}
