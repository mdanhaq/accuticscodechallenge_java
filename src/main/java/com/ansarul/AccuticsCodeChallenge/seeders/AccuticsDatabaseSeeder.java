package com.ansarul.AccuticsCodeChallenge.seeders;

import com.ansarul.AccuticsCodeChallenge.models.Campaigns;
import com.ansarul.AccuticsCodeChallenge.models.InputType;
import com.ansarul.AccuticsCodeChallenge.models.Inputs;
import com.ansarul.AccuticsCodeChallenge.models.Users;
import com.ansarul.AccuticsCodeChallenge.repositories.CampaignsRepository;
import com.ansarul.AccuticsCodeChallenge.repositories.InputsRepository;
import com.ansarul.AccuticsCodeChallenge.repositories.UsersRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

/**
 * Seeder component that adds data to PostgreSQL if it is empty. If you want an empty one change the boolean attribute
 * emptyDatabase to true.
 */

@Component
public class AccuticsDatabaseSeeder {

    private AccuticsDatabaseSeeder accuticsDatabaseSeeder;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private InputsRepository inputsRepository;

    @Autowired
    private CampaignsRepository campaignsRepository;

    private boolean emptyDatabase = false;

    @EventListener
    public void seedDatabase(ContextRefreshedEvent event){


        if(inputsRepository.findAll().isEmpty() && !emptyDatabase){
            seedInputs();
        }
        if(campaignsRepository.findAll().isEmpty() && !emptyDatabase){
            seedCampaigns();
        }
        if(usersRepository.findAll().isEmpty() && !emptyDatabase){
            seedUsers();
        }

    }

    private void seedUsers() {
        Users newUsersOne = new Users();
        newUsersOne.setUserName("Ansarul Haque");
        newUsersOne.setEmail("mdanhaq@gmail.com");
        usersRepository.save(newUsersOne);

        Users newUsersTwo = new Users();
        newUsersTwo.setUserName("Nicholas Cage");
        newUsersTwo.setEmail("nicholar@gmail.com");
        usersRepository.save(newUsersTwo);

        Users newUsersThree = new Users();
        newUsersThree.setUserName("Jesper Fenn");
        newUsersThree.setEmail("jesper@gmail.com");
        usersRepository.save(newUsersThree);

    }

    private void seedCampaigns() {
        Campaigns newCampaignsOne = new Campaigns();
        //newCampaignsOne.setUsers(usersRepository.getById(1));
        campaignsRepository.save(newCampaignsOne);

        Campaigns newCampaignsTwo = new Campaigns();
        //newCampaignsTwo.setUsers(usersRepository.getById(3));
        campaignsRepository.save(newCampaignsTwo);

        Campaigns newCampaignsThree = new Campaigns();
        //newCampaignsThree.setUsers(usersRepository.getById(2));
        campaignsRepository.save(newCampaignsThree);
    }

    private void seedInputs() {
        Inputs newInputsOne = new Inputs();
        newInputsOne.setInputType(InputType.campaign_name);
        newInputsOne.setValue("Unknown");
        //newInputsOne.setCampaigns(campaignsRepository.getById(2));
        inputsRepository.save(newInputsOne);

        Inputs newInputsTwo = new Inputs();
        newInputsTwo.setInputType(InputType.channel);
        newInputsTwo.setValue("Unknown");
        //newInputsTwo.setCampaigns(campaignsRepository.getById(1));
        inputsRepository.save(newInputsTwo);

    }

}
