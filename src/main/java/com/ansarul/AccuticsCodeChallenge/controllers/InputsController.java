package com.ansarul.AccuticsCodeChallenge.controllers;

import com.ansarul.AccuticsCodeChallenge.models.Campaigns;
import com.ansarul.AccuticsCodeChallenge.models.Inputs;
import com.ansarul.AccuticsCodeChallenge.repositories.CampaignsRepository;
import com.ansarul.AccuticsCodeChallenge.repositories.InputsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/inputs")
public class InputsController {

    @Autowired
    private InputsRepository inputsRepository;

    /**
     * Fetching all the current inputs from the database
     * @return : Returns a list of inputs
     */
    @GetMapping
    public ResponseEntity<List<Inputs>> getAllInputs(){
        List<Inputs> inputs = inputsRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(inputs, resp);
    }
}