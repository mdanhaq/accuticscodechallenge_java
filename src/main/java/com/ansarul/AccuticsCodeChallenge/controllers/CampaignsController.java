package com.ansarul.AccuticsCodeChallenge.controllers;

import com.ansarul.AccuticsCodeChallenge.models.Campaigns;
import com.ansarul.AccuticsCodeChallenge.repositories.CampaignsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/campaigns")
public class CampaignsController {

    @Autowired
    private CampaignsRepository campaignsRepository;

    /**
     * Fetching campaigns from the current database
     * @param page : Page number
     * @param sortBy : Sort by attributes
     * @return : List of campaigns with the specific number of elements
     */
    @GetMapping
    Page<Campaigns> getAllCampaigns(
            @RequestParam Optional<Integer> page
            ,@RequestParam Optional<String> sortBy
    ){
        return campaignsRepository.findAll(
                PageRequest.of(
                        page.orElse(0),
                        3
                        ,Sort.Direction.DESC, sortBy.orElse("campaignId")
                )
        );
    }

    /**
     * Adding a campaign to the database
     * @param campaigns: Campaigns
     * @return : New campaign object
     */
    @PostMapping
    public ResponseEntity<Campaigns> addCampaign(@RequestBody Campaigns campaigns){
        Campaigns add = campaignsRepository.save(campaigns);
        HttpStatus status= HttpStatus.CREATED;
        return new ResponseEntity<>(add, status);
    }


}
