package com.ansarul.AccuticsCodeChallenge.controllers;

import com.ansarul.AccuticsCodeChallenge.models.Users;
import com.ansarul.AccuticsCodeChallenge.repositories.UsersRepository;
import com.ansarul.AccuticsCodeChallenge.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/users")
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UsersService usersService;

    /**
     * Fetching all the current users from the database
     * @return : Returns a list of users
     */
    @GetMapping
    public ResponseEntity<List<Users>> getAllUsers(){
        List<Users> users = usersRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(users, resp);
    }

    /**
     * Adding a user to the database
     * @param users: User
     * @return : New user object
     */
    @PostMapping
    public ResponseEntity<Users> addUser(@RequestBody Users users){
        Users add = usersRepository.save(users);
        HttpStatus status= HttpStatus.CREATED;
        return new ResponseEntity<>(add, status);
    }

    /**
     * Fetch a user from the database with a given email
     * @param email: Email identifier
     * @return: Returns the user from given email
     */
    @GetMapping("/{email}")
    public ResponseEntity<Users> getUserByEmail(@PathVariable("email") String email){
        Users user = usersService.getUserByEmail(email);
        HttpHeaders header=new HttpHeaders();
        header.add("desc", "Getting book by email");
        return ResponseEntity.status(HttpStatus.OK).headers(header).body(user);
    }
}
