package com.ansarul.AccuticsCodeChallenge.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Campaigns {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int campaignId;
    /**
     * Several campaigns can be related to a single user, hence a link is created to the User entity
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users users;

    /**
     * @return : Returns a List with Strings representing user URLs
     */
    @JsonGetter("users")
    public String users() {
        if (users != null) {
            return "/api/v1/users/" + users.getUserId();
        } else {
            return null;
        }
    }

    /**
     * One campaign can have several inputs, hence a link is created to the Input entity
     */
    @OneToMany(mappedBy = "campaigns")
    List<Inputs> inputs;

    /**
     * @return : Returns a list with Strings representing input URLs
     */
    @JsonGetter("inputs")
    public List<String> inputs() {
        if (inputs != null) {
            return inputs.stream()
                    .map(input -> {
                        return "/api/v1/inputs/" + input.getInputId() ;
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public List<Inputs> getInputs() {
        return inputs;
    }

    public void setInputs(List<Inputs> inputs) {
        this.inputs = inputs;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }
}
