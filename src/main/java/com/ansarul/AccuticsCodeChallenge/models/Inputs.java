package com.ansarul.AccuticsCodeChallenge.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
public class Inputs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int inputId;

    private InputType inputType;
    private String value;
    /**
     * Several inputs can be related to a single campaign, hence a link is created to the Campaign entity
     */
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaigns campaigns;

    /**
     * @return : Returns a list with Strings representing campaigns URLs
     */
    @JsonGetter("")
    public String campaigns() {
        if (campaigns != null) {
            return "/api/v1/campaigns/" + campaigns.getCampaignId();
        } else {
            return null;
        }
    }

    public Campaigns getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(Campaigns campaigns) {
        this.campaigns = campaigns;
    }

    public int getInputId() {
        return inputId;
    }

    public void setInputId(int inputId) {
        this.inputId = inputId;
    }

    public InputType getInputType() {
        return inputType;
    }

    public void setInputType(InputType inputType) {
        this.inputType = inputType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
