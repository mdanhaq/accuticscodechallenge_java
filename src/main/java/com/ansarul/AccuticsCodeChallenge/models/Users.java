package com.ansarul.AccuticsCodeChallenge.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;


    private String userName;
    private String email;

    /**
     * One user can have several campaigns, hence a link is created to the Movie entity
     */
    @OneToMany(mappedBy = "users")
    List<Campaigns> campaigns;

    /**
     * @return Returns a list with Strings representing campaign URLs
     */
    @JsonGetter("campaigns")
    public List<String> campaigns() {
        if (campaigns != null) {
            return campaigns.stream()
                    .map(campaign -> {
                        return "/api/v1/campaigns/" + campaign.getCampaignId() ;
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public List<Campaigns> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Campaigns> campaigns) {
        this.campaigns = campaigns;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
