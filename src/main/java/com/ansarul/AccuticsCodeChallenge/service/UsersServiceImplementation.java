package com.ansarul.AccuticsCodeChallenge.service;

import com.ansarul.AccuticsCodeChallenge.models.Users;
import com.ansarul.AccuticsCodeChallenge.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImplementation implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Users getUserByEmail(String email) {
        return usersRepository.findAll()
                .stream()
                .filter((users) -> users.getEmail().equals(email))
                .findAny()
                .orElse(new Users());
    }
}
