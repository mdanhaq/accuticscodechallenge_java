package com.ansarul.AccuticsCodeChallenge.service;

import com.ansarul.AccuticsCodeChallenge.models.Users;

import java.util.List;

public interface UsersService {
    Users getUserByEmail(String email);
}
